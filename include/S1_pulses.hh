//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Apr 21 16:39:05 2022 by ROOT version 6.24/04
// from TTree S1_pulses/S1_pulses
// found on file: ../test.root
//////////////////////////////////////////////////////////

#ifndef S1_pulses_h
#define S1_pulses_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class S1_pulses {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Double_t        evt_triggerTS;
   Int_t           runID;
   Int_t           eventID;
   Int_t           triggerType;
   std::vector<float>   *S1pulseAreas;
   std::vector<float>   *S1maxCHFracs;
   std::vector<float>   *S1maxCHs;
   std::vector<bool>    *S1isHSC;

  // List of branches
   TBranch        *b_evt_triggerTS;   //!
   TBranch        *b_runID;   //!
   TBranch        *b_eventID;   //!
   TBranch        *b_triggerType;   //!
   TBranch        *b_S1pulseAreas;   //!
   TBranch        *b_S1maxCHFracs;   //!
   TBranch        *b_S1maxCHs;   //!
   TBranch        *b_S1isHSC;   //!

  S1_pulses(TTree *tree=0);
   virtual ~S1_pulses();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

inline S1_pulses::S1_pulses(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../test.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("../test.root");
      }
      f->GetObject("S1_pulses",tree);

   }
   Init(tree);
}

inline S1_pulses::~S1_pulses()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

inline Int_t S1_pulses::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
inline Long64_t S1_pulses::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

inline void S1_pulses::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   S1pulseAreas = 0; 
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("evt_triggerTS", &evt_triggerTS, &b_evt_triggerTS);
   fChain->SetBranchAddress("runID", &runID, &b_runID);
   fChain->SetBranchAddress("eventID", &eventID, &b_eventID);
   fChain->SetBranchAddress("triggerType", &triggerType, &b_triggerType);
   fChain->SetBranchAddress("S1pulseAreas", &S1pulseAreas, &b_S1pulseAreas);
   fChain->SetBranchAddress("S1maxCHs", &S1maxCHs, &b_S1maxCHs);
   fChain->SetBranchAddress("S1maxCHFracs", &S1maxCHFracs, &b_S1maxCHFracs);
   fChain->SetBranchAddress("S1isHSC", &S1isHSC, &b_S1isHSC);
   Notify();
}

inline Bool_t S1_pulses::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

inline void S1_pulses::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
inline Int_t S1_pulses::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
