ROOTCFLAGS    = $(shell root-config --cflags)
ROOTGLIBS     = $(shell root-config --glibs)

CXX            = g++
CXXFLAGS       = -fPIC -Wall -O3 -g
CXXFLAGS       += $(filter-out -stdlib=libc++ -pthread , $(ROOTCFLAGS))
GLIBS          = $(filter-out -stdlib=libc++ -pthread , $(ROOTGLIBS))

INCLUDEDIR       = ./include/
SRCDIR           = ./src/
CXX             += -I$(INCLUDEDIR) -I.
OUTOBJ	         = ./obj/

CC_FILES := $(wildcard src/*.cc)
HH_FILES := $(wildcard include/*.hh)
OBJ_FILES := $(addprefix $(OUTOBJ),$(notdir $(CC_FILES:.cc=.o)))
DICT_FILES := $(wildcard include/*.pcm)

all: get_spikes.x veto_spikes.x

get_spikes.x:  $(SRCDIR)get_spikes.cc $(HH_FILES)
	$(CXX) $(CXXFLAGS) -o get_spikes.x $(GLIBS) $ $<
	touch get_spikes.x

veto_spikes.x:  $(SRCDIR)veto_spikes.cc $(HH_FILES)
	$(CXX) $(CXXFLAGS) -o veto_spikes.x $(GLIBS) $ $<
	touch veto_spikes.x

clean:
	rm -f $(OUTOBJ)*.o
	rm -rf *.dSYM
	rm -f get_spikes.x
	rm -f veto_spikes.x
