#include "TH1D.h"
#include "TH2D.h"
#include "TMultiGraph.h"
#include "TLine.h"
#include "TLatex.h"
#include "TTreeIndex.h"
#include "TGraph.h"
#include "TF1.h"
#include <iostream>
#include <algorithm>
#include <stdlib.h>
#include <numeric>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <iomanip>

#include "include/S1_pulses.hh"

using namespace std;

int main(int argc, char* argv[]){

  char inputFileName[400];
  char outputFileName[400];
  char exclusionFileName[400];
  
  int mult = 5;

  if ( argc < 5 ){
    cout << "Error at Input: please specify input/output .root files ";
    cout << "Example:   ./get_spikes.x -i input.root -o output.root" << endl;
    return 0;
  }

  bool b_input  = false;
  bool b_out    = false;
  bool b_text   = false;
  bool b_mult   = false;

  for (int i=1;i<argc-1;i++){
    if (strncmp(argv[i],"-i",2)==0){
      sscanf(argv[i+1],"%s", inputFileName);
      b_input = true;
    }
    if (strncmp(argv[i],"-o",2)==0){
      sscanf(argv[i+1],"%s", outputFileName);
      b_out = true;
    }
    if (strncmp(argv[i],"-t",2)==0){
      sscanf(argv[i+1],"%s", exclusionFileName);
      b_text = true;
    }
    if (strncmp(argv[i],"-m",2)==0){
      sscanf(argv[i+1],"%d", &mult);
      b_mult = true;
    }
  }
  
  if(!b_input) std::cout << "Error at Input: please specify  input file (-i flag)" << std::endl;
  if(!b_out)   std::cout << "Error at Input: please specify output file (-o flag)" << std::endl;
  if(!b_text)   std::cout << "Error at Input: please specify exclusion text file (-t flag)" << std::endl;
  if(!b_input || !b_out || !b_text) return 0;

  std::cout << "Using " << mult << " sigma threshold" << std::endl;

  TFile* f = new TFile(inputFileName, "READ");
  if(!f){
    std::cout << "Error: unable to open input file " << inputFileName << std::endl;
    return 0;
  }

  TTree* T = (TTree*) f->Get("S1_pulses");
  if(!T){
    std::cout << "Error: cannot find tree S1_pulses in " << inputFileName << std::endl;
    return 0;
  }

  S1_pulses* data = (S1_pulses*)new S1_pulses(T);

  int nentries = T->GetEntries();

  double TS       = 0.;
  int runID       = 0;
  int eventID     = 0;
  int triggerType = 0;
  std::vector<float> pulseAreas;
  std::vector<float> maxCHFracs;
  std::vector<float> maxCHss;
  std::vector<bool>  isHSC;

  double relTS = 0.;
  
  double t0       = 1640277471;// + 1.E-9*972458560;
  double offset   = 0.; //788893200.;
  // T->BuildIndex("runID","eventID");
  // TTreeIndex *index = (TTreeIndex*)T->GetTreeIndex();
  // for (int i = 0; i <index->GetN(); i++ ){

  // Get end of counting
  T->GetEntry(nentries-1);
  double tf       = data->evt_triggerTS;
  int run_f       = data->runID;
  int nbins       = (tf-t0)/5.;
  int nbins_big = (int)(nbins/240); // 20 minute bins
  double walltime_s = tf-t0;
  double walltime_hr = walltime_s / 60. / 60.;

  std::cout << "Assumes already sorted by time" << std::endl;
  std::cout << "Will require " << nbins << " time bins" << std::endl;
  
  vector<int> npulse(nbins, 0);
  vector<int> npulse_isHSC(nbins, 0);
  vector<int> npulse_isPMT199(nbins, 0);
  vector<int> ntrig(nbins, 0);
  vector<double> rate(nbins, 0.);
  vector<double> rate_isHSC(nbins, 0.);
  vector<double> rate_isPMT199(nbins, 0.);
  vector<std::pair<int,int>> events(nbins, std::make_pair(0,0));


  vector<int> npulse_big(nbins_big, 0);
  vector<int> ntrig_big(nbins_big, 0);
  vector<double> rate_big(nbins_big, 0.);

  vector<int> npulse_isHSC_big(nbins_big, 0);
  vector<double> rate_isHSC_big(nbins_big, 0.);

  vector<int> npulse_isPMT199_big(nbins_big, 0);
  vector<double> rate_isPMT199_big(nbins_big, 0.);
  
  // Diagnostic histograms
  TH1* rate_dist = new TH1D("rate_dist",";Rate [Hz];Bins", 500,0.,10000.);
  double sf = nbins*5 / 1000;
  TH1* rate_time = new TH1D("rate_time",";Rel. Time [s];Rate", nbins, 0., nbins*5.);
  TH1* rate_time_abs = new TH1D("rate_time_abs",";Trig Time [s];Rate", nbins, t0, tf);
  TH1* rate_time_abs_isHSC = new TH1D("rate_time_abs_isHSC",";Trig Time [s];Rate", nbins, t0, tf);
  TH1* rate_time_abs_isPMT199 = new TH1D("rate_time_abs_isPMT199",";Trig Time [s];Rate", nbins, t0, tf);
  TH1* rate_time_abs_largebins = new TH1D("rate_time_abs_largebins",";Trig Time [s];Rate", nbins_big, t0, tf);
  TH1* rate_time_abs_isHSC_largebins = new TH1D("rate_time_abs_isHSC_largebins",";Trig Time [s];Rate", nbins_big, t0, tf);
  TH1* rate_time_abs_isPMT199_largebins = new TH1D("rate_time_abs_isPMT199_largebins",";Trig Time [s];Rate", nbins_big, t0, tf);
  TH1* spike_times = new TH1D("spike_times",";Rel. Time [s]; Counts", 1000, 0., nbins*5.);
  TH1* spike_times_abs = new TH1D("spike_times_abs",";Trig Time [s]; Counts", 1000, t0, tf);
  
  for (int i = 0; i < nentries; i++ ){
    T->GetEntry(i);

    // Get all variables
    TS          = data->evt_triggerTS;
    relTS       = TS - t0;
    runID       = data->runID;
    eventID     = data->eventID;
    triggerType = data->triggerType;

    int index = (floor) (relTS / 5);
    int index_big = (floor) (relTS / 1200);
    
    ntrig[index] += 1;
    ntrig_big[index_big] += 1;
    events[index] = std::make_pair(runID,eventID);

    // Loop through pulses and count them
    for (unsigned int ipulse = 0; ipulse < data->S1pulseAreas->size(); ipulse++){
      npulse[index] += 1;
      npulse_big[index_big] += 1;
      float this_pulseArea = data->S1pulseAreas->at(ipulse);
      bool isHSC = data->S1maxCHFracs->at(ipulse)*this_pulseArea > (pow(this_pulseArea,0.4) + 1);
      bool isPMT199 = ((int)data->S1maxCHs->at(ipulse) == 199);
      if (isHSC) {
	npulse_isHSC[index] += 1;
	npulse_isHSC_big[index_big] += 1;
      }
      if (isPMT199) {
	npulse_isPMT199[index] += 1;
	npulse_isPMT199_big[index_big] += 1;
      }
    }
  }


  // Loop through time bins and calculate the rate
  for (unsigned int ibin = 0; ibin < ntrig.size(); ibin++){
    if ( ntrig[ibin] > 0) {
      rate[ibin] = (double)npulse[ibin]/(ntrig[ibin]*4.5*pow(10,-3)*0.7);
      rate_isHSC[ibin] = (double)npulse_isHSC[ibin]/(ntrig[ibin]*4.5*pow(10,-3)*0.7);
      rate_isPMT199[ibin] = (double)npulse_isPMT199[ibin]/(ntrig[ibin]*4.5*pow(10,-3)*0.7);
      rate_dist->Fill(rate[ibin]);
      int this_bin = rate_time->GetXaxis()->FindBin(ibin*5.);
      rate_time->SetBinContent(this_bin,rate[ibin]);
      rate_time_abs->SetBinContent(this_bin,rate[ibin]);
      rate_time_abs_isHSC->SetBinContent(this_bin,rate_isHSC[ibin]);
      rate_time_abs_isPMT199->SetBinContent(this_bin,rate_isPMT199[ibin]);
    }
    else {
      rate[ibin] = 0;
      rate_isHSC[ibin] = 0;
      rate_isPMT199[ibin] = 0;
    }
  }
  // do this for the big bins
  for (unsigned int ibin = 0; ibin < ntrig_big.size(); ibin++){
    if ( ntrig_big[ibin] > 0) {
      rate_big[ibin] = (double)npulse_big[ibin]/(ntrig_big[ibin]*4.5*pow(10,-3)*0.7);
      double this_err = sqrt((double)npulse_big[ibin])/(ntrig_big[ibin]*4.5*pow(10,-3)*0.7);
      rate_isHSC_big[ibin] = (double)npulse_isHSC_big[ibin]/(ntrig_big[ibin]*4.5*pow(10,-3)*0.7);
      rate_isPMT199_big[ibin] = (double)npulse_isPMT199_big[ibin]/(ntrig_big[ibin]*4.5*pow(10,-3)*0.7);
      int this_bin_abs = rate_time_abs_largebins->GetXaxis()->FindBin(ibin*1200.+t0 - offset);
      rate_time_abs_largebins->SetBinContent(this_bin_abs,rate_big[ibin]);
      rate_time_abs_largebins->SetBinError(this_bin_abs,this_err);
      rate_time_abs_isHSC_largebins->SetBinContent(this_bin_abs,rate_isHSC_big[ibin]);
      rate_time_abs_isPMT199_largebins->SetBinContent(this_bin_abs,rate_isPMT199_big[ibin]);
    }
    else {
      rate_big[ibin] = 0;
      rate_isHSC_big[ibin] = 0;
      rate_isPMT199_big[ibin] = 0;
    }
  }
  
  // Filter vector for rates above 0
  std::vector<double> good_rate;
  std::copy_if(rate.begin(), rate.end(), std::back_inserter(good_rate), [](double i) {
        return i > 0;
  });

  int nremoved = rate.size() - good_rate.size();
  std::cout << nremoved << " bins removed" << std::endl;

  // Fit distribution
  TF1* fgauss = new TF1("g1","gaus(0)",50,250);
  rate_dist->Fit(fgauss,"R");
  double gmean = fgauss->GetParameter(1);
  double gsigma = fgauss->GetParameter(2);

  // Calculate mean and standard deviation
  double sum = std::accumulate(good_rate.begin(), good_rate.end(), 0.0);
  double mean = sum / good_rate.size();
  
  std::vector<double> diff(good_rate.size());
  std::transform(good_rate.begin(), good_rate.end(), diff.begin(), [mean](double x) { return x - mean; });
  double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
  double stdev = std::sqrt(sq_sum / good_rate.size());
  //double thresh = mult * stdev + mean;

  double thresh = mult * gsigma + gmean;

  std::cout << ">> Mean rate: " << mean << std::endl;
  std::cout << ">> Standard deviation: " << stdev << std::endl;

  std::cout << ">> Gaussian mu: " << gmean << std::endl;
  std::cout << ">> Gaussian sigma: " << gsigma << std::endl;
  std::cout << ">> Threshold: " << thresh << std::endl;

  // Filter for bins which should be cut out
  std::vector<int> cut_bins;
  std::vector<std::pair<int,int>> cut_events;
  for (unsigned int ibin = 0; ibin < rate.size(); ibin++){
    // if (ibin == 1288870)
    //   std::cout << "Rate: " << rate[ibin] << std::endl;
    if (rate[ibin] > thresh) {
      double perc_error  = sqrt(npulse[ibin]);
      perc_error /= npulse[ibin];
      // if (ibin == 1288870)
      // 	std::cout << "Percent error: " << perc_error << std::endl;
      if (rate[ibin]*(1-perc_error) > thresh){
	cut_bins.push_back(ibin);
	cut_events.push_back(events[ibin]);
      }
    }
  }
  std::cout << ">> N(cut): " << cut_bins.size() << std::endl;


  // Calculate excluded time periods
  double start = -999;
  double end   = -999;
  double walltime_cut_s = 0.;
  double before_cut_s = 5;
  double after_cut_s  = 50;
  int nspike = 0;

  ofstream myfile;
  myfile.open (exclusionFileName);
  
  for (unsigned int icut = 0; icut < cut_bins.size(); icut++){
    double this_bin = cut_bins[icut] * 5.;
    spike_times->Fill(this_bin);
    spike_times_abs->Fill(this_bin+t0);
    if (start < 0)
      start = this_bin;
    if (end < 0)
      end = this_bin;
    if (((this_bin - end) > 10)){
      start -= before_cut_s;
      end += after_cut_s;
      double abs_start = start + t0;
      double abs_end   = end + t0;
      int diff = abs_end-abs_start;
      walltime_cut_s += diff;
      std::cout << ">> Spike " << nspike << ", "
		<< std::fixed << std::setprecision(1) << (int)abs_start << " " << (int)abs_end << std::endl;
      std::cout << std::endl;
      myfile << std::fixed << std::setprecision(1) << (int)abs_start << " " << (int)abs_end << "\n";

      nspike++;
      start = this_bin;
      end   = this_bin;
    }
    else
      end   = this_bin;
    std::cout << "Run: " << cut_events[icut].first << ", Event: " << cut_events[icut].second << std::endl;
    if (icut == (cut_bins.size()-1)){
      start -= before_cut_s;
      end += after_cut_s;
      double abs_start = start + t0;
      double abs_end   = end + t0;
      int diff = abs_end-abs_start;
      walltime_cut_s += diff;
      std::cout << ">> Spike " << nspike << ", "
		<< std::fixed << std::setprecision(1) << (int)abs_start << " " << (int)abs_end << std::endl;
      std::cout << std::endl;
      myfile << std::fixed << std::setprecision(1) << (int)abs_start << " " << (int)abs_end << "\n";

      nspike++;
      start = this_bin;
      end   = this_bin;
    }
  }

  double walltime_cut_hr = walltime_cut_s / 60. / 60.;
  std::cout << "\n--------------------------" << std::endl;
  std::cout << "Summary " << std::endl;
  std::cout << "--------------------------" << std::endl;
  std::cout << "Walltime cut (hours):   " << std::fixed << std::setprecision(6) << walltime_cut_hr << std::endl;
  std::cout << "Walltime cut (fraction): " << walltime_cut_hr/(walltime_hr) << std::endl;
  std::cout << "N(spikes):              " << nspike << std::endl;
  
  TFile* fout = new TFile(outputFileName, "RECREATE");
  fout->cd();
  rate_dist->Write();
  spike_times->Write();
  spike_times_abs->Write();
  rate_time->Write();
  rate_time_abs->Write();
  rate_time_abs_largebins->Write();
  rate_time_abs_isHSC->Write();
  rate_time_abs_isPMT199->Write();
  rate_time_abs_isHSC_largebins->Write();
  rate_time_abs_isPMT199_largebins->Write();
  fout->Close();

  myfile.close();
    
}
