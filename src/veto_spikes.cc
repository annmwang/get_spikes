#include "TH1D.h"
#include "TH2D.h"
#include "TMultiGraph.h"
#include "TLine.h"
#include "TLatex.h"
#include "TTreeIndex.h"
#include "TGraph.h"
#include "TF1.h"
#include <iostream>
#include <algorithm>
#include <stdlib.h>
#include <numeric>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include "include/S1_pulses.hh"

using namespace std;

int main(int argc, char* argv[]){

  char inputFileName[400];
  char outputFileName[400];
  char exclusionFileName[400];
  

  if ( argc < 5 ){
    cout << "Error at Input: please specify input/output .root files ";
    cout << "Example:   ./veto_spikes.x -i input.root -o output.root" << endl;
    return 0;
  }

  bool b_input  = false;
  bool b_out    = false;
  bool b_text   = false;

  for (int i=1;i<argc-1;i++){
    if (strncmp(argv[i],"-i",2)==0){
      sscanf(argv[i+1],"%s", inputFileName);
      b_input = true;
    }
    if (strncmp(argv[i],"-o",2)==0){
      sscanf(argv[i+1],"%s", outputFileName);
      b_out = true;
    }
    if (strncmp(argv[i],"-t",2)==0){
      sscanf(argv[i+1],"%s", exclusionFileName);
      b_text = true;
    }
  }
  
  if(!b_input) std::cout << "Error at Input: please specify  input file (-i flag)" << std::endl;
  if(!b_out)   std::cout << "Error at Input: please specify output file (-o flag)" << std::endl;
  if(!b_text)   std::cout << "Error at Input: please specify exclusion text file (-t flag)" << std::endl;
  if(!b_input || !b_out || !b_text) return 0;

  TFile* f = new TFile(inputFileName, "READ");
  if(!f){
    std::cout << "Error: unable to open input file " << inputFileName << std::endl;
    return 0;
  }

  TTree* T = (TTree*) f->Get("S1_pulses");
  if(!T){
    std::cout << "Error: cannot find tree S1_pulses in " << inputFileName << std::endl;
    return 0;
  }

  S1_pulses* data = (S1_pulses*)new S1_pulses(T);

  int nentries = T->GetEntries();

  double TS       = 0.;
  int runID       = 0;
  int eventID     = 0;
  int triggerType = 0;
  std::vector<float> pulseAreas;
  std::vector<float> maxCHFracs;
  std::vector<float> maxCHss;
  std::vector<bool>  isHSC;

  double relTS = 0.;
  
  double t0       = 1640277471;// + 1.E-9*972458560;
  double offset   = 0.; //788893200.;

  // Get end of counting
  T->GetEntry(nentries-1);
  double tf       = data->evt_triggerTS;
  int nbins       = (tf-t0)/5.;
  int nbins_big = (int)(nbins/240); // 20 minute bins
  double walltime_s = tf-t0;
  double walltime_hr = walltime_s / 60. / 60.;

  std::cout << "Assumes already sorted by time" << std::endl;
  std::cout << "Will require " << nbins << " time bins" << std::endl;
  
  vector<int> npulse(nbins, 0);
  vector<int> npulse_isHSC(nbins, 0);
  vector<int> npulse_isPMT199(nbins, 0);
  vector<int> ntrig(nbins, 0);
  vector<double> rate(nbins, 0.);
  vector<double> rate_isHSC(nbins, 0.);
  vector<double> rate_isPMT199(nbins, 0.);
  vector<std::pair<int,int>> events(nbins, std::make_pair(0,0));


  vector<int> npulse_big(nbins_big, 0);
  vector<int> ntrig_big(nbins_big, 0);
  vector<double> rate_big(nbins_big, 0.);

  vector<int> npulse_isHSC_big(nbins_big, 0);
  vector<double> rate_isHSC_big(nbins_big, 0.);

  vector<int> npulse_isPMT199_big(nbins_big, 0);
  vector<double> rate_isPMT199_big(nbins_big, 0.);
  
  // Diagnostic histograms
  TH1* rate_dist = new TH1D("rate_dist",";Rate [Hz];Bins", 500,0.,10000.);
  double sf = nbins*5 / 1000;
  TH1* rate_time = new TH1D("rate_time",";Rel. Time [s];Rate", nbins, 0., nbins*5.);
  TH1* rate_time_abs = new TH1D("rate_time_abs",";Trig Time [s];Rate", nbins, t0, tf);
  TH1* rate_time_abs_isHSC = new TH1D("rate_time_abs_isHSC",";Trig Time [s];Rate", nbins, t0, tf);
  TH1* rate_time_abs_isPMT199 = new TH1D("rate_time_abs_isPMT199",";Trig Time [s];Rate", nbins, t0, tf);
  TH1* rate_time_abs_largebins = new TH1D("rate_time_abs_largebins",";Trig Time [s];Rate", nbins_big, t0, tf);
  TH1* rate_time_abs_isHSC_largebins = new TH1D("rate_time_abs_isHSC_largebins",";Trig Time [s];Rate", nbins_big, t0, tf);
  TH1* rate_time_abs_isPMT199_largebins = new TH1D("rate_time_abs_isPMT199_largebins",";Trig Time [s];Rate", nbins_big, t0, tf);
  
  TH1* pulseArea_random = new TH1D("pulseArea_random",";Pulse Area [phd];S1s", 500,0.,500.);
  TH1* pulseArea_random_HSCcut = new TH1D("pulseArea_random_HSCcut",";Pulse Area [phd];S1s", 500,0.,500.);
  TH1* pulseArea_random_vetospikes = new TH1D("pulseArea_random_vetospikes",";Pulse Area [phd];S1s", 500,0.,500.);
  TH1* pulseArea_random_HSCcut_vetospikes = new TH1D("pulseArea_random_HSCcut_vetospikes",";Pulse Area [phd];S1s", 500,0.,500.);

  TH1* pulseArea = new TH1D("pulseArea",";Pulse Area [phd];S1s", 500,0.,500.);
  TH1* pulseArea_vetospikes = new TH1D("pulseArea_vetospikes",";Pulse Area [phd];S1s", 500,0.,500.);

  // Parse exclusion text file
  ifstream exclFile(exclusionFileName);
  std::vector<string> lines;
  string line;
  std::vector<std::pair<int,int>> periods;
  while (getline(exclFile, line)){
    std::stringstream ss(line);
    string start, end;
    ss >> start;
    ss >> end;
    periods.push_back(std::make_pair(stoi(start),stoi(end)));
  }

  for (int i = 0; i < nentries; i++ ){
    T->GetEntry(i);

    // Get all variables
    TS          = data->evt_triggerTS;
    relTS       = TS - t0;
    runID       = data->runID;
    eventID     = data->eventID;
    triggerType = data->triggerType;

    // Loop through pulses and count them
    for (unsigned int ipulse = 0; ipulse < data->S1pulseAreas->size(); ipulse++){
      float this_pulseArea = data->S1pulseAreas->at(ipulse);
      bool isHSC = data->S1maxCHFracs->at(ipulse)*this_pulseArea > (pow(this_pulseArea,0.4) + 1);
      if (triggerType == 32){
	pulseArea_random->Fill(this_pulseArea);
	if (!isHSC)
	  pulseArea_random_HSCcut->Fill(this_pulseArea);
      }
      pulseArea->Fill(this_pulseArea);
    }
    
    bool excluded = false;
    for ( auto period : periods ) {
      if (TS > period.first && TS < period.second) {
	excluded = true;
      }
    }
      
    if (excluded == true)
      continue;
    
    int index = (floor) (relTS / 5);
    int index_big = (floor) (relTS / 1200);
    
    ntrig[index] += 1;
    ntrig_big[index_big] += 1;
    events[index] = std::make_pair(runID,eventID);

    // Loop through pulses and count them
    for (unsigned int ipulse = 0; ipulse < data->S1pulseAreas->size(); ipulse++){
      npulse[index] += 1;
      npulse_big[index_big] += 1;
      float this_pulseArea = data->S1pulseAreas->at(ipulse);
      bool isHSC = data->S1maxCHFracs->at(ipulse)*this_pulseArea > (pow(this_pulseArea,0.4) + 1);
      bool isPMT199 = ((int)data->S1maxCHs->at(ipulse) == 199);
      if (isHSC) {
	npulse_isHSC[index] += 1;
	npulse_isHSC_big[index_big] += 1;
      }
      if (isPMT199) {
	npulse_isPMT199[index] += 1;
	npulse_isPMT199_big[index_big] += 1;
      }
      pulseArea_vetospikes->Fill(this_pulseArea);
      if (triggerType == 32) {
	pulseArea_random_vetospikes->Fill(this_pulseArea);
	if (!isHSC)
	  pulseArea_random_HSCcut_vetospikes->Fill(this_pulseArea);
      }
    }
  }

  // Loop through time bins and calculate the rate
  for (unsigned int ibin = 0; ibin < ntrig.size(); ibin++){
    if ( ntrig[ibin] > 0) {
      rate[ibin] = (double)npulse[ibin]/(ntrig[ibin]*4.5*pow(10,-3)*0.7);
      rate_isHSC[ibin] = (double)npulse_isHSC[ibin]/(ntrig[ibin]*4.5*pow(10,-3)*0.7);
      rate_isPMT199[ibin] = (double)npulse_isPMT199[ibin]/(ntrig[ibin]*4.5*pow(10,-3)*0.7);
      rate_dist->Fill(rate[ibin]);
      int this_bin = rate_time->GetXaxis()->FindBin(ibin*5.);
      rate_time->SetBinContent(this_bin,rate[ibin]);
      rate_time_abs->SetBinContent(this_bin,rate[ibin]);
      rate_time_abs_isHSC->SetBinContent(this_bin,rate_isHSC[ibin]);
      rate_time_abs_isPMT199->SetBinContent(this_bin,rate_isPMT199[ibin]);
    }
    else {
      rate[ibin] = 0;
      rate_isHSC[ibin] = 0;
      rate_isPMT199[ibin] = 0;
    }
  }
  // do this for the big bins
  for (unsigned int ibin = 0; ibin < ntrig_big.size(); ibin++){
    if ( ntrig_big[ibin] > 0) {
      rate_big[ibin] = (double)npulse_big[ibin]/(ntrig_big[ibin]*4.5*pow(10,-3)*0.7);
      rate_isHSC_big[ibin] = (double)npulse_isHSC_big[ibin]/(ntrig_big[ibin]*4.5*pow(10,-3)*0.7);
      rate_isPMT199_big[ibin] = (double)npulse_isPMT199_big[ibin]/(ntrig_big[ibin]*4.5*pow(10,-3)*0.7);
      int this_bin_abs = rate_time_abs_largebins->GetXaxis()->FindBin(ibin*1200.+t0 - offset);
      rate_time_abs_largebins->SetBinContent(this_bin_abs,rate_big[ibin]);
      rate_time_abs_isHSC_largebins->SetBinContent(this_bin_abs,rate_isHSC_big[ibin]);
      rate_time_abs_isPMT199_largebins->SetBinContent(this_bin_abs,rate_isPMT199_big[ibin]);
    }
    else {
      rate_big[ibin] = 0;
      rate_isHSC_big[ibin] = 0;
      rate_isPMT199_big[ibin] = 0;
    }
  }
  
  
  TFile* fout = new TFile(outputFileName, "RECREATE");
  fout->cd();
  rate_dist->Write();
  rate_time->Write();
  rate_time_abs->Write();
  rate_time_abs_largebins->Write();
  rate_time_abs_isHSC->Write();
  rate_time_abs_isPMT199->Write();
  rate_time_abs_isHSC_largebins->Write();
  rate_time_abs_isPMT199_largebins->Write();
  pulseArea_random->Write();
  pulseArea_random_vetospikes->Write();
  pulseArea_random_HSCcut_vetospikes->Write();
  pulseArea_random_HSCcut->Write();
  pulseArea->Write();
  pulseArea_vetospikes->Write();
  fout->Close();

}
